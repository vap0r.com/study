#include <stdio.h>
#include <stdlib.h>

#define DEBUG_ELEMENTS 10 // >=1: debug on | 0: debug off

struct person_type {
    char name[20+1];
    int age;
    int pn;
    struct person_type * next;
    struct person_type * prev;
};

/* *** function prototypes *** */
struct person_type * debug_mode(struct person_type * );
void clearscreen ();
void print_menu ();
void press_enter ();
struct person_type * free_all(struct person_type *);
struct person_type * create_element(struct person_type * );
void show_elements(struct person_type * );
struct person_type * delete_element(struct person_type * );
void search_pn(struct person_type * );
void search_name(struct person_type * );
void search_age(struct person_type * );
void search_element(struct person_type * );
void edit_element(struct person_type *);
struct person_type * swap_elements_ui(struct person_type *);
struct person_type * swap_elements(struct person_type *, struct person_type *, struct person_type *);
struct person_type * bubblesort(struct person_type *);
void savelist(struct person_type *);
struct person_type * loadlist(struct person_type *);

void main () {
    char choice;
    struct person_type * fptr;
    fptr = NULL;
    /* If debug mode create 10 Test User */
    if (DEBUG_ELEMENTS > 0) fptr = debug_mode(fptr);

    clearscreen ();
    print_menu ();
    choice = getchar();
    getchar ();                 /* read CR from keyboard buffer */
    while (choice != '0') {
        clearscreen();
        switch (choice) {
            case '1': printf ("Add element:\n");
                fptr = create_element(fptr);
                break;
            case '2': printf ("Elements:\n");
                show_elements(fptr);
                break;
            case '3': printf ("Remove element:\n");
                fptr = delete_element(fptr);
                break;
            case '4': printf ("search element\n");
                search_element(fptr);
                break;
            case '5': printf ("swap element\n");
                fptr = swap_elements_ui(fptr);
                break;
            case '6': printf ("sort elements\n");
                fptr = bubblesort(fptr);
                break;
            case '7': printf ("edit an element\n");
                edit_element(fptr);
                break;
            case '8': printf ("post in social media\n");
                break;
            case '9': printf ("Delete all elements:\n");
                fptr = free_all(fptr);
                break;
            case 'S':
            case 's': printf ("save to disk\n");
                break;
            case 'L':
            case 'l': printf ("load from disk\n");
                break;
            default : break;
        } /* end of switch */
        press_enter();
        clearscreen();
        print_menu ();
        choice = getchar();
        getchar ();                  /* read CR from keyboard buffer */
    } /* end of while */
    printf ("Thanks for using the modern file-card system.\n");
}

void clearscreen () 
{
    system("clear");
}

void print_menu () 
{
    printf (" The modern file-card system\n");
    printf ("-----------------------------\n\n");
    printf ("1. add / insert element\n");
    printf ("2. show / print list\n");
    printf ("3. remove element\n");
    printf ("4. search element in list\n");
    printf ("5. move / swap elements\n");
    printf ("6. sort the list\n");
    printf ("7. edit an element\n");
    printf ("8. post in Social Media    (WIP)\n");
    printf ("9. free / delete all\n");
    printf ("S. save list on disk       (WIP)\n");
    printf ("L. load list from disk     (WIP)\n");
    printf ("0. quit program\n\n");
    printf ("Please make a choice: ");
}

void press_enter () 
{
    char ch=0;
    printf ("\nPlease press ENTER to continue ...\n");
    while (ch!=10) ch=getchar();
}

struct person_type * debug_mode(struct person_type * fptr)
{
    struct person_type * ptr;
    int n = 999+DEBUG_ELEMENTS;

    for (int i; i<DEBUG_ELEMENTS; i++) {
        ptr = (struct person_type *) malloc(sizeof(struct person_type));
        ptr->next = fptr;
        ptr->prev = NULL;

        if (ptr->next != NULL) ptr->next->prev = ptr;

        ptr->pn = n;
        strcpy(ptr->name, "TestUser");
        ptr->age = 23;

        fptr = ptr;
        n--;
    }
    return(fptr);
}

struct person_type * free_all(struct person_type * fptr) 
{
    if (fptr==NULL) {
        printf("No Elements to delete\n");
        return fptr;
    }
    printf("deleted all...\n");
    struct person_type * ptr;
    do {
        ptr = fptr->next;
        free(fptr);
        fptr = ptr;
    } while(ptr != NULL);
    return(NULL);
}

struct person_type * create_element(struct person_type * fptr) 
{
    struct person_type * rptr = fptr;
    struct person_type * ptr;
    while(rptr->next != NULL) {
        rptr = rptr->next;
    };
    ptr = (struct person_type *) malloc(sizeof(struct person_type));
    ptr->next = NULL;
    ptr->prev = rptr;
    rptr->next = ptr;

    //if (ptr->next != 0) ptr->next->prev = ptr;

    printf("Enter PN: ");
    scanf("%d", &ptr->pn);
    printf("Insert Student name: ");
    scanf("%s", ptr->name);
    printf("Insert Student Age: ");
    scanf("%d", &ptr->age);

    printf("Successfully added Filecard!\n");
    return(fptr);
}

void show_elements(struct person_type * ptr) 
{
    if (ptr == NULL){
        printf("*Filecard System is empty*");
        return(NULL);
    }
    int i=0;
    printf(" %-3s - %-4s - %-20s - %3s\n", "N", " PN", "    Name", "Age");
    do {
        printf("[%02d] | %4d | %-20s | %3d\n", i, ptr->pn, ptr->name, ptr->age);
        ptr = ptr->next;
        i++;
    } while (ptr != NULL);
}

struct person_type * delete_element(struct person_type * fptr) 
{
    show_elements(fptr);
    int n;
    printf("Element [number] to delete: ");
    scanf("%d",&n);

    struct person_type * ptr;

    ptr = fptr;
    for (int i=0;i<n;i++) {
        ptr = ptr->next;
    }
    if (ptr->next == NULL && ptr->prev == NULL) {
        fptr = NULL;
    } else if (ptr->next == NULL) {
        ptr->prev->next = NULL;
    } else if (ptr->prev == NULL) {
        ptr->next->prev = NULL;
        fptr = fptr->next;
    } else {
        ptr->next->prev = ptr->prev;
        ptr->prev->next = ptr->next;
    }

    free((void *) ptr);
    return(fptr);
}

void search_pn(struct person_type * fptr)
{
    int n=0;
    int pn;
    printf("Enter PN to search for: ");
    scanf("%d", &pn);
    do {
        if ( fptr->pn == pn ) {
            printf("PN: %d | Name: %s | Age: %d\n", fptr->pn, fptr->name, fptr->age);
            n++;
        }
        fptr = fptr->next;
    } while (fptr != NULL);

    getchar();
    printf("Found %d entries!", n);
}

void search_name(struct person_type * fptr)
{
    struct person_type * ptr;
    ptr = fptr;
    int n=0;
    char name[20+1];
    printf("Enter Name to search for: ");
    scanf("%s", name);
    printf("\n");
    do {
        if (strncmp(ptr->name, name) == 0) {
            printf("PN: %d | Name: %s | Age: %d\n", ptr->pn, ptr->name, ptr->age);
            n++;
        }
        ptr = ptr->next;
    }while (ptr != NULL);

    getchar();
    printf("\nFound %d entries!", n);
}

void search_age(struct person_type * fptr)
{
    int n=0;
    int age;
    printf("Enter age to search for: ");
    scanf("%d", &age);
    do {
        if ( fptr->age == age ) {
            printf("PN: %d | Name: %s | Age: %d\n", fptr->pn, fptr->name, fptr->age);
            n++;
        }
        fptr = fptr->next;
    } while (fptr != NULL);

    getchar();
    printf("Found %d entries!", n);
}

void search_element(struct person_type * fptr)
{
    printf("[1] to search for PN\n[2] to search for Name\n[3] to search for age\n");
    printf("Enter search field: ");
    int n;
    scanf("%d", &n);
    switch(n) {
        case 1: search_pn(fptr);
            break;
        case 2: search_name(fptr);
            break;
        case 3: search_age(fptr);
            break;
    }
}

void edit_element(struct person_type * fptr)
{
    show_elements(fptr);
    int n;
    printf("Element [n] to edit: ");
    scanf("%d",&n);
    struct person_type * ptr = fptr;
    for (int i=0;i<n;i++) {
        ptr = ptr->next;
    }
    printf("Enter PN [%d]: ", ptr->pn);
    scanf("%d", &ptr->pn);
    printf("Insert Student name [%s]: ", ptr->name);
    scanf("%s", ptr->name);
    printf("Insert Student Age [%d]: ", ptr->age);
    scanf("%d", &ptr->age);
}

struct person_type * swap_elements_ui(struct person_type * fptr) 
{
    show_elements(fptr);
    int m,n;
    printf("Element 1 [n] to edit: ");
    scanf("%d", &m);
    printf("Element 2 [n] to edit: ");
    scanf("%d", &n);
    int i;
    struct person_type * ptr1, * ptr2, * helpp, * helpn, * helpp1, * helpn1;
    ptr1 = fptr;
    ptr2 = fptr;
    if (m>n) {
        m^=n^=m^=n;
    }
    for (i=0;i<m;i++) {
        ptr1 = ptr1->next;
    };
    for (i=0;i<n;i++) {
        ptr2 = ptr2->next;
    };    
    return (swap_elements(fptr, ptr1, ptr2));
}

struct person_type * swap_elements(struct person_type * fptr, struct person_type * ptr1, struct person_type * ptr2) 
{
    struct person_type * helpp, * helpp1, * helpn, * helpn1;
    if (ptr1->next == ptr2) {
        helpp = ptr1->prev;
        ptr1->prev = ptr2;
        helpn = ptr2->next;
        ptr2->next = ptr1;

        ptr1->next = helpn;
        if (helpp) {
            helpp->next = ptr2;
        } else {fptr = ptr2;}
        ptr2->prev = helpp;
        if (helpn) helpn->prev = ptr1; 
    } else {
        helpp = ptr1->prev;
        helpp1 = ptr2->prev;
        helpn = ptr1->next;
        helpn1 = ptr2->next;
        ptr1->prev = helpp1;
        ptr2->prev = helpp;
        ptr1->next = helpn1;
        ptr2->next = helpn;
        if (helpp) {
            helpp->next = ptr2;
        } else {fptr = ptr2;}
        if (helpp1) {
            helpp1->next = ptr1;
        } else {fptr = ptr1;}
        if (helpn) helpn->prev = ptr2;
        if (helpn1) helpn1->prev = ptr1;
    }
    return(fptr);
}

struct person_type * bubblesort(struct person_type * fptr)
{
    struct person_type * ptr=fptr;
    int n=0;
    while (ptr) {
        ptr = ptr->next;
        n++;
    }
    ptr = fptr;
    int i,j;
    for (i=n-1;i>0;i--) {
        for (j=0;j<i;j++) {
            if (ptr->pn > ptr->next->pn) fptr = swap_elements(fptr, ptr, ptr->next);
            ptr = ptr->next;
        }
        ptr = fptr;
    }
    show_elements(fptr);
    return (fptr);
}

void savelist(struct person_type * fptr) 
{
}

struct person_type * loadlist(struct person_type * fptr) 
{
}