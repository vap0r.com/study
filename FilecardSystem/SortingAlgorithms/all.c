// Sorting Algorithms

// Test Array
int array[] = {42, 16, 5, 5, 17, 69, 420, 110, 1};

// prototypes
void bubblesort(int array[]);
void printarray(int array[], int len);

void main() {
    bubblesort(array);
}

void bubblesort(int array[]) {
    int i,j,help;
    int len = sizeof(array);
    for (i=len-1;i>0;i--)
    {
        for (j=0; j<i;j++) {
            if (array[j+1] < array[j]) {
                help = array[j];
                array[j] = array[j+1];
                array[j+1] = help;
            }
        }
    }
    printarray(array, len);
}

void printarray(int array[], int len) {
    int i;
    printf("[");
    for (i=0;i<len;i++) {
        printf("%d,", array[i]);
    }
    printf("]\n");
}