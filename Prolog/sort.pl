
% *** Helping terms for sort algorithms ***

% concatenate two lists
conc([], L, L).
conc([X|R], L, [X|S]) :- conc(R, L, S).

% split a list, K: [all elements smaller than/ equal to E], G: [all elements greater than E]
split([], E, [], []).
split([X|R], E, [X|K], G) :- X =< E, split(R, E, K, G).
split([X|R], E, K, [X|G]) :- X > E, split(R, E, K, G).

% breaks list in two lists
half([], [], []).
half([X|[]], [X], []).
half([X,Y|R], [X|LX], [Y|LY]) :- half(R, LX, LY).

% merge lists
mrg([], L, L).
mrg(L, [], L).
mrg([X|R], [Y|T], [X|L]) :- X=<Y, mrg(R,[Y|T],L).
mrg([X|R], [Y|T], [Y|L]) :- X>Y, mrg([X|R],T,L).


% *** Sort Algorithm ***

% Quicksort
qsort([], []).
qsort([E|R], L) :- split(R, E, K, G), qsort(K, SK), qsort(G, SG), conc(SK, [E|SG], L).

% Mergesort
mrgsort([], []).
mrgsort([A], [A]).
mrgsort(R, L) :- half(R, S, T), mrgsort(S, L1), mrgsort(T, L2), mrg(L1, L2, L).