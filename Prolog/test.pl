/* conc: concatenate two lists*/
conc([], L, L).
conc(L, [], L).
conc([X|R], L, [X|S]) :- conc(R, L, S).

/* zipper: zips two lists */
zipper(L,[],L).
zipper([], L, L).
zipper([X|RX], [Y|RY], [X,Y|R]) :- zipper(RX, RY, R).

/* pali: creates a palindrome from given list */
pali([], []).
pali([X|R], [X|S]) :- pali(R,T),conc(T, [X], S).

/* rev: reverse list */
rev([],[]).
rev([X|R], S) :- rev(R, UR),conc(UR,[X],S).

% Sums up the values of the numbers at the odd positions
oddpossum([], 0).
oddpossum([X], X).
oddpossum([X,Y|R], S) :- oddpossum(R, T), S is X+T.

% Sums up all positive numbers
possum([], 0).
possum([X|R], S) :- X>0, possum(R, T), S is X+T.
possum([X|R], S) :- possum(R, S).