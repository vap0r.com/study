# Study Projects

## Filecardsystem

*  Language: C
*  Files: main.c (Stores Elements as array); main_dynamic.c (Stores Elements as double linked list)

A Filecardsystem to store persons as structs in a double linked list. Works as command line program.

## Prolog

### test.pl
- Language: Prolog (swi-prolog)

Prolog File for test terms.

### sort.pl
- Language: Prolog (swi-prolog)

Prolog Terms to sort lists.
